from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import loader
from django.utils import timezone
from time import strftime
from .models import Blogs
from .models import Comments

from random import randint

LOREM_IPSUM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Proin vel urna pulvinar, convallis metus vitae, tempus dolor.
Suspendisse accumsan ante sit amet pulvinar porttitor.
Fusce pulvinar mi sit amet urna fringilla tincidunt.
Quisque gravida libero ac nibh bibendum, eget ultrices nibh blandit.
Cras condimentum lacus vel justo scelerisque placerat.
Sed ut ipsum aliquam, vulputate odio id, vestibulum erat.
Curabitur lacinia justo volutpat posuere condimentum.
Nulla ultricies elit in diam maximus, id fermentum quam ultrices.
Aliquam gravida felis a sem fringilla rutrum.
Integer quis massa at quam varius volutpat.
Curabitur sagittis mi ut mi dictum molestie.
Vivamus mattis orci id risus eleifend, id semper arcu mattis.
Suspendisse ullamcorper nibh ac mauris semper venenatis.
Quisque sed felis nec tortor sagittis maximus et ac felis.
Etiam ac turpis rhoncus, convallis lacus id, laoreet nulla.
Morbi laoreet massa aliquam ornare convallis.
Curabitur non lorem quis odio imperdiet vestibulum.
Nullam ac eros interdum, aliquet ipsum nec, lobortis sem.
Morbi hendrerit nisl in lorem luctus eleifend.
Etiam congue eros at mauris dapibus, ac dignissim dui egestas.
Curabitur condimentum libero in tortor molestie maximus.
Nulla eget mauris in metus faucibus imperdiet.
Nam in ante quis turpis pellentesque hendrerit ac et turpis.
Vestibulum mattis sem a mollis iaculis.
Curabitur ultricies eros ultricies justo lacinia sollicitudin.
Curabitur ac nunc nec nisi condimentum accumsan eget et urna.
Mauris posuere velit et nulla consequat ullamcorper.
Fusce nec lectus quis augue cursus pharetra.
Vestibulum fringilla dolor non cursus rhoncus.
Donec aliquam nisi ac suscipit scelerisque.
Pellentesque vulputate erat quis tellus luctus volutpat.
Mauris id magna quis urna blandit faucibus.
Donec aliquam elit ac nisl suscipit fringilla.
Nulla tempor purus ac lorem suscipit lacinia.
In ac odio ac magna mollis mattis.
Nam consectetur elit at sagittis sagittis.
Fusce cursus ipsum id metus varius fringilla.
Praesent imperdiet elit non neque fermentum finibus.
Proin tristique sem rutrum, accumsan sem non, sollicitudin mi.
Nulla vel nulla a quam vulputate consectetur vel sed tortor.
Cras id tortor eu sem malesuada sagittis.
Mauris in lacus quis dolor volutpat venenatis.
Proin sodales justo eu tempus laoreet.
Aenean a velit eu risus dictum efficitur id in elit.
Aliquam scelerisque risus eget tellus condimentum, ac sollicitudin diam blandit.
Sed suscipit mauris vitae neque malesuada congue.
Nulla ac sapien eu orci pellentesque tincidunt vel sit amet est.
Praesent venenatis dolor eu ornare mollis.
Maecenas volutpat tortor non mi aliquet placerat.
Maecenas in orci vitae sapien dignissim placerat.
Suspendisse lacinia enim sit amet magna tristique hendrerit pretium quis nisl.
Vestibulum bibendum leo et lobortis mollis.
Vivamus non sapien feugiat, imperdiet sem sed, dapibus augue.
Vestibulum in dui vehicula, pretium enim pellentesque, placerat velit.
Maecenas efficitur arcu eu molestie luctus.
Sed luctus eros vitae nisi commodo, eu faucibus arcu mollis.
Suspendisse tempor lectus sed suscipit facilisis.
Sed interdum massa non ante scelerisque dictum sed vitae dolor.
Sed a libero sit amet diam lacinia bibendum ac vel purus.
Aliquam non dui eget ex pulvinar mollis a sit amet velit.
Donec non ex at nibh aliquet venenatis.
Cras nec erat auctor, consectetur turpis a, scelerisque nunc.
Vestibulum vitae nibh ornare, dictum lacus ac, fermentum nisi.
Proin vel turpis consectetur, tempus risus ut, scelerisque neque.
Phasellus sit amet arcu non lacus suscipit viverra.
In sodales nisi at est porta, sed sodales erat mollis.
Vestibulum lacinia dolor ut nulla finibus tempor.
Etiam sed eros aliquam urna scelerisque ornare.
Aenean at risus nec neque hendrerit varius nec quis nulla.
Mauris auctor velit sit amet libero tempus dapibus.
Pellentesque egestas urna et diam consequat, vitae ultrices nulla consequat.
Integer ut massa nec sapien auctor laoreet.
Pellentesque eget sem non dolor iaculis tempor.
Curabitur luctus est vitae pulvinar varius.
Vivamus sed mauris eu nibh vehicula blandit maximus vel velit.
Vestibulum congue ipsum a nisi consectetur pretium.
Proin ac sapien eleifend, ultrices felis at, molestie nunc.
Fusce non velit gravida, tincidunt ipsum quis, elementum urna.
Integer imperdiet lorem non neque condimentum lacinia.
In dapibus ipsum dapibus, ornare arcu gravida, tristique diam.
In scelerisque ipsum quis viverra auctor.
Integer ullamcorper lectus quis diam placerat tincidunt.
Praesent volutpat ligula in nunc consequat, in bibendum ligula sollicitudin.
Vestibulum in diam a sapien tincidunt dapibus quis ac leo.
Nunc vitae libero nec nulla vulputate egestas at quis lacus.
Suspendisse scelerisque erat nec mattis feugiat.
Donec volutpat augue eget augue sodales tristique.
Ut facilisis leo in sapien elementum pulvinar.
Pellentesque vitae augue eu velit iaculis bibendum.
Nulla rutrum tortor in libero maximus interdum.
Pellentesque non sapien sodales, dictum nisi vitae, ultrices purus.
Quisque tincidunt lacus vitae lectus viverra consectetur.
Nullam vitae urna ultricies, pellentesque lacus consequat, vulputate ex.
Praesent condimentum orci tincidunt urna gravida viverra.
Nulla ut urna sed tellus dignissim cursus eget ut urna.
Cras id velit ut ipsum placerat gravida.
Maecenas sodales orci in felis pretium, in placerat massa bibendum.
Phasellus eleifend arcu a egestas varius.
Curabitur tempus diam sit amet orci pellentesque dapibus.
Pellentesque ultricies leo quis nunc vestibulum, non fringilla quam egestas.
In vitae enim vestibulum, hendrerit ipsum in, laoreet purus.
Vivamus quis sapien non risus scelerisque eleifend ut quis eros.
Fusce ornare mauris a risus efficitur, varius cursus mauris laoreet.
Donec id nibh tristique, dapibus velit vitae, pretium massa.
Nunc malesuada lectus aliquet felis rutrum sollicitudin.
Phasellus lacinia nisl nec sapien lobortis, nec dignissim eros convallis.
Nulla sit amet elit vitae sapien congue sodales vitae nec sapien.
Praesent a enim convallis, mollis tellus quis, tincidunt augue.
Pellentesque accumsan ante a leo suscipit imperdiet.
Morbi eget nisl at urna cursus facilisis.
Donec elementum urna nec odio semper luctus.
Donec ullamcorper enim pharetra lacus efficitur pellentesque.
Sed cursus turpis gravida, interdum turpis sit amet, tincidunt nisi.
Morbi consequat felis eu risus laoreet, vel imperdiet ipsum semper.
In tempor augue eget mauris consequat, vel elementum neque elementum.
Aliquam quis metus eu lacus maximus tristique eget non libero.
Donec vestibulum odio et tortor pellentesque pharetra.
Praesent eu magna sit amet dui varius volutpat eget eu metus.
Sed accumsan sapien a risus pharetra ultricies.
Maecenas semper quam nec quam dapibus cursus.
Proin gravida sapien id bibendum feugiat.
Pellentesque vitae enim ultricies, facilisis ante non, semper ipsum.
Nulla ac elit finibus, congue enim sit amet, mollis velit.
Cras pulvinar erat ultricies diam dignissim, sit amet gravida ex blandit.
Integer congue nisl vel feugiat venenatis.
Aenean sit amet neque laoreet, euismod est ut, ultricies elit.
Sed sit amet ipsum laoreet, maximus nisi id, hendrerit urna.
Vestibulum sollicitudin ipsum a nunc efficitur, ac consectetur ligula dapibus.
Aliquam ultricies libero a nisi tincidunt, ut elementum velit tempor.
Proin eu dui ac lorem congue luctus.
Integer consequat dui ut lectus aliquet dictum.
Pellentesque gravida risus finibus finibus auctor.
Nam aliquam eros et nunc ornare iaculis sit amet vel nunc.
Vivamus dignissim dolor at varius commodo.
Sed ut nisi eu sapien feugiat blandit.
Proin elementum libero feugiat, venenatis sapien ut, condimentum nunc.
Integer in justo pretium, hendrerit purus eget, aliquet mauris.
Vestibulum iaculis sem non metus tincidunt feugiat.
Ut eu quam nec turpis sollicitudin tincidunt id ut tortor.
Phasellus sit amet augue finibus, facilisis ipsum mollis, varius lacus.
Suspendisse aliquet ante eu turpis lobortis hendrerit.
Nunc fringilla leo vel tempus pellentesque.
Ut vitae dui pharetra, iaculis purus vitae, viverra lorem.
Suspendisse ac sem at dui cursus vehicula sit amet non justo.
Aliquam eget massa bibendum, bibendum dui eu, condimentum nisl.
Sed et nulla et velit pharetra sodales.
Pellentesque bibendum ligula et magna tincidunt, eu tincidunt sapien hendrerit.
Donec ut tortor vestibulum, aliquam massa et, placerat ex.
Vestibulum ultrices dolor quis congue vehicula.
Duis ornare purus eget auctor tincidunt.
Quisque tristique orci in nisi mollis ullamcorper.
Proin vehicula odio quis purus pretium vulputate.
Maecenas eu orci sed orci scelerisque fringilla.
Curabitur consequat ligula sit amet lacus finibus, sit amet dictum nisi aliquet.
Aliquam a purus finibus eros suscipit varius.
Praesent a leo vitae ipsum ultricies sodales.
Cras bibendum eros non sem viverra tincidunt.
Nunc vitae mauris efficitur, dictum quam id, laoreet ante.
Maecenas blandit libero sit amet orci pharetra, ac pharetra orci vehicula.
Ut tincidunt augue ut elit sodales, non congue arcu consequat.
Proin id metus vel quam lobortis faucibus.
Donec gravida turpis sit amet dui hendrerit iaculis.
Sed luctus massa non ex placerat, ac aliquam nibh vestibulum.
Donec vel eros sed arcu dapibus interdum.
Nunc non ligula blandit, cursus risus ut, cursus magna.
Nam dictum nunc nec nisi maximus pretium.
Aliquam convallis orci nec feugiat dictum.
Pellentesque cursus diam a arcu commodo, sit amet eleifend metus bibendum.
Phasellus et ligula maximus, laoreet nulla sit amet, sodales dolor.
Maecenas in nisl eget orci lobortis bibendum.
Sed ullamcorper sem quis diam euismod, eget iaculis quam egestas.
Quisque a diam vestibulum, condimentum magna sed, tincidunt augue.
Nullam facilisis mi eget justo consequat, eu tempus leo faucibus.
Aliquam aliquet nisi non orci luctus, nec semper nulla ultricies.
Suspendisse molestie nulla at ligula interdum luctus.
Phasellus quis nunc nec libero ornare varius facilisis ut nunc.
Integer finibus dolor ac felis imperdiet aliquet.
Cras ac mauris non leo eleifend molestie.
Vestibulum sed purus fringilla, scelerisque nisi nec, luctus eros.
Curabitur eu lorem ut dolor dignissim tempus.
Suspendisse interdum nunc ac eros aliquam feugiat.
Sed a diam eu libero viverra bibendum id sed mi.
Vestibulum vitae orci tristique, mollis lorem non, pharetra neque.
Suspendisse vestibulum arcu fringilla nisi finibus, et faucibus odio euismod.
Cras eu dolor ut augue posuere pretium eu eget lectus.
Praesent auctor urna vitae vestibulum fermentum.
Pellentesque semper sapien ac massa ullamcorper facilisis.
Proin faucibus augue facilisis lorem ornare, id placerat tellus cursus.
Sed gravida ante id velit lobortis, ac egestas massa semper.
Praesent porttitor lectus a arcu tempor, a dictum arcu volutpat.
Curabitur consequat elit pulvinar eros porta bibendum.
Donec vel orci venenatis mauris vehicula lobortis a sit amet nisl.
Ut fringilla ante quis dui luctus cursus.
Mauris bibendum ex eget consequat fermentum.
Mauris semper felis nec fringilla cursus.
Duis auctor ante ut risus laoreet faucibus.
Aenean id ipsum vitae augue fermentum egestas sit amet id neque.
Mauris molestie felis vitae sagittis maximus.
Quisque et augue id ex scelerisque eleifend eget ut lorem.
Nunc ornare nunc in lacus varius, vitae suscipit ipsum placerat.
Morbi cursus elit eget metus posuere, in euismod lorem vestibulum.
Morbi vitae ex nec tortor rhoncus volutpat vitae id eros.
Maecenas et nibh lobortis ex interdum tempus sed vitae dolor.
Sed laoreet sem a viverra scelerisque.
Mauris ut libero eu erat euismod congue vitae ac urna.
Sed ac sapien quis nunc condimentum rutrum in nec erat.
Fusce dapibus elit a neque lacinia, quis suscipit sapien ullamcorper.
Phasellus mattis leo a mauris dapibus, sed scelerisque ante iaculis.
Donec dictum est vel arcu aliquam rhoncus.
Proin scelerisque massa id sem luctus posuere.
Aliquam non sapien quis nulla rhoncus commodo.
Nullam eget mi varius risus finibus pulvinar.
Morbi ullamcorper lectus ut lacus rutrum, sed sagittis felis viverra.
Maecenas dictum urna aliquam nisl aliquam eleifend.
Nam auctor felis non massa viverra, nec blandit erat ullamcorper.
Etiam tempus felis eu erat efficitur blandit scelerisque non lorem.
Mauris in lorem eget quam efficitur vulputate.
Ut lacinia odio ac dignissim varius.
Nulla placerat libero vitae nisl feugiat vulputate.
Sed imperdiet turpis ut pulvinar finibus.
Donec finibus ipsum id ligula ullamcorper, in iaculis diam volutpat.
Nullam aliquam dui sed ex interdum, vel finibus metus finibus.
Quisque non magna viverra, mattis libero sed, eleifend nulla.
Maecenas faucibus est a quam tincidunt varius.
Fusce tristique est id egestas dictum.
Proin condimentum nunc vitae urna pharetra dignissim.
Nunc venenatis mi condimentum, suscipit tellus vel, mattis mi.
In accumsan dolor quis dui vehicula, id consectetur dolor eleifend.
Cras eu arcu id mi lobortis gravida at vel metus.
Sed sed urna sit amet diam vestibulum aliquam.
Nunc luctus nulla eget turpis cursus tempor.
Nullam placerat dui sit amet mattis semper.
Aliquam sed lacus vitae mi luctus dapibus.
Pellentesque eget lacus maximus, consequat odio non, eleifend massa.
Fusce vel mi sed ante venenatis lobortis eu vitae justo.
Curabitur sit amet mi at felis consectetur fringilla.
Donec sollicitudin orci nec orci interdum, quis aliquam orci elementum.
Proin ac lacus ullamcorper, condimentum mauris vitae, tempus metus.
Nam faucibus massa tempor justo posuere tincidunt.
Quisque convallis elit id viverra sodales.
Nunc pulvinar eros aliquet velit aliquet, pretium hendrerit nunc mattis.
Integer vel tellus molestie, feugiat ex at, ornare dolor.
Pellentesque pharetra sapien nec dictum imperdiet.
Curabitur volutpat ex quis luctus fermentum.
Curabitur placerat risus id libero aliquet dignissim.
Vivamus molestie orci quis sodales condimentum.
Donec eu sapien et enim fringilla ultrices sed nec tellus.
Donec ut nisi sed tellus tincidunt iaculis.
Vestibulum in urna sit amet velit semper venenatis vitae nec purus.
Nunc cursus ex ac cursus ultricies.
Proin condimentum dolor ut nibh lacinia cursus.
Sed id felis nec urna aliquet imperdiet ut vitae enim.
Etiam sit amet dolor nec lectus efficitur efficitur vitae vitae massa.
Proin tempor arcu id commodo semper.
Quisque vitae mi aliquam turpis malesuada posuere.
Vestibulum placerat eros sed sapien lobortis sagittis.
Ut vel risus ultricies lorem varius tempor.
Vivamus consequat turpis vitae egestas pretium.
Duis ultricies mi vel nunc ultricies ultricies.
Sed vel mauris non mauris luctus tempor.
Pellentesque venenatis elit id suscipit commodo.
Sed facilisis justo vitae velit lacinia, at ornare ex commodo.
Nunc mollis augue eget mauris eleifend, id commodo sem dictum.
Proin vitae urna fringilla, lobortis lacus sed, hendrerit nisl.
In eu leo quis nulla commodo iaculis.
Vivamus nec magna non justo tristique tempus.
Donec in leo vel libero condimentum bibendum.
Vivamus aliquam lorem vel justo consectetur interdum.
Maecenas vestibulum mi vel ipsum tempor, consectetur hendrerit magna eleifend.
Ut sed urna vitae dolor luctus finibus.
Nulla et ipsum sed eros ullamcorper congue quis eu justo.
Sed imperdiet nunc euismod neque aliquam, ut sodales ex posuere.
Donec in quam et felis tincidunt elementum quis id eros.
Integer vitae lectus tempor mauris convallis vulputate.
Etiam ac tortor vitae lorem blandit rutrum.
Nunc venenatis purus id ipsum dictum commodo.
Vivamus vehicula tortor a ultrices fringilla.
Vestibulum scelerisque lorem eget porta pellentesque.
Aliquam at quam a urna lacinia tincidunt.
Integer at mauris varius, vulputate libero et, tristique purus.
Aenean non nunc ac mauris tincidunt sodales eget eget lectus.
Sed placerat purus eget dui tristique fringilla.
Curabitur venenatis nunc at ex tincidunt, id aliquet enim imperdiet.
Morbi mollis purus vel condimentum malesuada.
Fusce sit amet erat ut nunc congue scelerisque.
Aliquam ullamcorper enim eget sapien euismod, eu ultrices dui elementum.
Suspendisse eget tellus sodales, eleifend mi sollicitudin, efficitur dolor.
Curabitur ac ligula eget ligula elementum hendrerit.
Praesent rutrum libero sed dui convallis feugiat.
Nam hendrerit magna vel ipsum venenatis tincidunt.
Praesent et felis euismod, molestie sem vel, euismod quam.
Donec ultrices arcu id ipsum accumsan, eget egestas nisi egestas.
Morbi ultricies lectus ac rhoncus pharetra.
Phasellus vel lacus ultricies, venenatis magna non, ornare nunc.
Nullam bibendum nibh et diam faucibus, sit amet malesuada quam tempor.
Nullam lacinia odio id porttitor volutpat.
Vestibulum efficitur est vel mauris porttitor, mattis posuere libero ullamcorper.
Praesent non nisi quis orci mollis pulvinar non eget urna.
Donec imperdiet justo ut purus commodo, in maximus turpis euismod.
Donec quis nulla ac mauris ultrices venenatis et imperdiet metus.
Curabitur interdum urna non erat commodo suscipit.
Donec vel nisi sed dolor vestibulum fermentum.
In maximus metus in orci lobortis, et ornare diam vulputate.
Duis sit amet urna sed massa pretium euismod.
Quisque ac nisi in leo efficitur ullamcorper.
Nullam dapibus odio non tristique sagittis.
Fusce vitae felis eget sapien auctor egestas.
Nam vitae lorem eu lacus tempor pharetra.
In fermentum ex quis tellus scelerisque, a finibus orci posuere.
Mauris tincidunt dui non diam vehicula fermentum.
Pellentesque pulvinar risus ac dui convallis vestibulum.
Sed at diam volutpat, aliquam nisi vel, luctus magna.
Ut condimentum metus ut sagittis condimentum.
Vivamus quis lectus ac arcu pharetra tincidunt.
Pellentesque sit amet sem tincidunt, rhoncus libero vitae, volutpat arcu.
Integer non est eget massa sollicitudin laoreet.
Praesent aliquam nunc a nibh vulputate aliquam.
Morbi tincidunt est eu tempus pharetra.
Nulla a enim ut felis pulvinar bibendum.
Quisque maximus turpis feugiat gravida blandit.
Donec hendrerit ante ut sapien varius fringilla.
Nunc id odio quis leo pharetra placerat.
Curabitur ut mauris quis enim tempus rutrum nec at magna.
Mauris tempor dolor eu ligula egestas, sed ultrices eros iaculis.
Quisque et nunc ullamcorper purus finibus laoreet in eget tortor.
Aliquam dignissim mauris eu nibh lobortis, quis commodo nisl sollicitudin.
Duis blandit ipsum ac fermentum congue.
Sed vitae lectus nec odio lobortis consectetur sed id orci.
Quisque in eros scelerisque, congue sem imperdiet, ornare turpis.
Curabitur dapibus metus quis urna lacinia, eu convallis ipsum euismod.
Phasellus quis urna quis mi dictum porttitor sed sit amet mauris.
Vestibulum ac ligula suscipit, efficitur massa ut, interdum turpis.
Suspendisse vel massa malesuada, pulvinar sem nec, varius justo.
Pellentesque molestie nisl in purus ullamcorper, at pharetra tortor viverra.
Mauris ut enim sed erat posuere condimentum id sit amet odio.
Ut ac neque vel ante auctor pulvinar.
Duis a felis porta sapien tincidunt faucibus.
Mauris porta arcu nec arcu porta, nec fermentum ante dapibus.
Nullam dapibus ligula eget dolor convallis laoreet.
In vehicula lectus ac dui convallis tempus.
Fusce nec urna sed nulla elementum suscipit.
Vestibulum egestas velit vitae leo ultricies efficitur.
Cras efficitur turpis non lacus hendrerit, a lacinia velit porttitor.
Pellentesque malesuada erat finibus, auctor ipsum id, semper nulla.
Suspendisse euismod nulla eu tempor posuere.
Pellentesque tristique ex a posuere bibendum.
Sed vitae eros et ipsum efficitur placerat.
Nam eu ante laoreet, pellentesque tortor nec, tincidunt orci.
Duis feugiat sem non dui faucibus, non molestie odio vehicula.
Sed non diam id massa consequat dignissim.
Vivamus gravida tortor vel est ultrices, id pharetra tortor sodales.
Phasellus id turpis tincidunt, iaculis arcu ullamcorper, tristique quam.
Donec sed enim sit amet neque suscipit porttitor.
Praesent viverra orci ac lacinia efficitur.
Maecenas posuere erat non ante sollicitudin hendrerit.
Vivamus interdum risus eu velit maximus malesuada.
Proin sodales magna vitae velit suscipit elementum commodo sit amet tortor.
Duis mollis ligula at lacus ullamcorper dignissim.
Suspendisse laoreet mauris at metus egestas, id placerat nulla feugiat.
Phasellus aliquam dui mollis orci rhoncus, in aliquam nunc ullamcorper.
Pellentesque ac arcu id risus aliquet tempor.
Morbi nec lacus nec nulla ullamcorper rhoncus in id purus.
Nullam volutpat diam non magna tempor fermentum.
Duis interdum diam ut ex interdum dictum.
Maecenas vitae diam et sem pharetra commodo.
Phasellus nec velit finibus lectus facilisis vehicula non imperdiet libero.
Vivamus et arcu eget mauris vehicula viverra.
Cras efficitur felis non sapien laoreet rhoncus.
Sed fermentum metus eget diam malesuada condimentum.
Praesent sit amet magna sed sapien vestibulum imperdiet posuere ac diam.
Etiam pretium libero a ligula tempus, ut egestas libero ultricies."""


def index(request):
    latest_post = Blogs.objects.order_by('-date')[:3]
    template = loader.get_template('blog/index.html')
    context = {
        'current_time': strftime('%c'),
        'newestPost': latest_post
    }
    return HttpResponse(template.render(context, request))


def archive(request):
    latest_post = Blogs.objects.order_by('-date')
    template = loader.get_template('blog/archive.html')
    context = {
        'current_time': strftime('%c'),
        'newestPost': latest_post
    }
    return HttpResponse(template.render(context, request))


def detail(request, blog_id):
    blog = Blogs.objects.get(pk=blog_id)
    template = loader.get_template('blog/detail.html')
    comments = Comments.objects.filter(blog_id=blog_id).order_by('-id')
    context = {
        'current_time': strftime('%c'),
        'this_blog': blog,
        'comments': comments
    }
    return HttpResponse(template.render(context, request))


def make_comment(request, blog_id):
    comment_author = request.POST['name']
    comment = request.POST['comment']
    email = request.POST['email']
    blog = Blogs.objects.get(pk=blog_id)
    newComment = Comments(comment_author=comment_author, comment=comment, email=email, blog=blog, date=timezone.now())
    newComment.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def bio(request):
    template = loader.get_template('blog/bio.html')
    context = {
        'current_time': strftime('%c')
    }
    return HttpResponse(template.render(context, request))


def techtips(request):
    template = loader.get_template('blog/techtips.html')
    context = {
        'current_time': strftime('%c')
    }
    return HttpResponse(template.render(context, request))


def init(request):
    Blogs.objects.all().delete()

    for b in range(5):
        sb = str(b)
        blog = Blogs(title='Blog Post #' + sb,
                     post=LOREM_IPSUM[:randint(300, len(LOREM_IPSUM))],
                     date=timezone.now(),
                     author='L.Ipsum, PhD')
        blog.save()

        for c in range(randint(b, b + b)):
            sc = str(c)
            blog.comments_set.create(
                comment_author='Gu, Internet Troll',
                email='wdoubleyou@dot.daught',
                comment='*monkey noises*',
                date=timezone.now()
            )

    return HttpResponseRedirect("/blog/")
