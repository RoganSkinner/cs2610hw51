from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('archive/', views.archive, name='archive'),
    path('bio/', views.bio, name='bio'),
    path('techtips/', views.techtips, name='techtips'),
    path('<int:blog_id>/', views.detail, name='detail'),
    path('<int:blog_id>/comment', views.make_comment, name='comment'),
    path('init/', views.init, name='init')
]

