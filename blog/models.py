import datetime

from django.db import models

from django.utils import timezone


class Blogs(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    post = models.TextField(max_length=5000)
    date = models.DateTimeField('published')

    def num_comments(self):
        return Comments.objects.filter(blog=self).count()
    childComments = property(num_comments)

    def __str__(self):
        return self.post

    def recent_publish(self):
        return self.date >= timezone.now() - datetime.timedelta(days=1)


class Comments(models.Model):
    blog = models.ForeignKey(Blogs, on_delete=models.CASCADE)
    comment_author = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    comment = models.TextField(max_length=1000)
    date = models.DateTimeField('published')

    def __str__(self):
        return self.comment
